section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rdi, rax
    mov rax, 60
    syscall



; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; just as in the book
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length ; len(str) -> rax
    pop rdi

    mov rdx, rax
    mov rsi, rdi ; arg is pointing at str start
    mov rax, 1 ; sys_write call number
    mov rdi, 1 ; write to stdout
    syscall

    xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; have to use this to make a pointer on the char with rsp
    
    mov rax, 1 ; sys_write call number
    mov rdi, 1 ; write to stdout
    mov rsi, rsp ; :/
    mov rdx, 1
    syscall
    pop rdi

    xor rax, rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA ; reusable code!
    call print_char

    xor rax, rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi ; arg -> rax
    mov r8, 10 ; divider -> r8
    mov rdi, rsp
    sub rsp, 21 ; 2^64 = 18446744073709551616 => need 20 bytes to store it as string with base 10 and 1 for zero terminator
    dec rdi
    mov byte[rdi], 0
    .loop:
        xor rdx, rdx ; rdx := 0
        div r8
        add dl, '0' 
        dec rdi
        mov byte[rdi], dl
        cmp rax, 0
        jne .loop
    call print_string
    add rsp, 21
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jns .poz
    jmp .neg


    .poz:
        jmp print_uint

    .neg:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ; rdi, rsi - pointers
    xor rcx, rcx
    .loop:
        mov r8b, byte[rdi + rcx]
        mov r9b, byte[rsi + rcx]
        inc rcx
        cmp r8b, 0
        je .end
        cmp r9b, 0
        je .end

        cmp r8b, r9b
        jne .end
        jmp .loop
    .end:
        cmp r8b, r9b
        je .equals
        mov rax, 0
        ret
    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ; rdi - buf pointer
    ; rsi - buf size
    mov r8, rdi
    mov r9, rsi
    xor rcx, rcx
    mov r10, rcx
    .loop:

        call read_char ; !!!! CALL, NOT JUMP !!!!


        cmp rax, 0x20
        je .end_or_skip
        cmp rax, 0x9
        je .end_or_skip
        cmp rax, 0xA
        je .end_or_skip
        cmp rax, 0
        je .end


        mov byte[r8+r10], al
        inc r10
        cmp r10, r9 
        jge .error
        jmp .loop

    .error:
        mov rax, 0
        ret

    .end_or_skip:
        cmp r10, 0
        jne .end
        jmp .loop

    .end:
        mov byte[r8+r10], 0
        mov rax, r8
        mov rdx, r10
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ; rdi - pointer
    ; rcx - index
    ; rax - summator
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    mov r9, 10
    .loop:
        mov r8b, byte[rdi+rcx]
        
        sub r8b, '0'
        cmp r8b, 9
        ja .bad_char
        inc rcx ; inc only if char is a number
        mul r9
        add rax, r8
        jmp .loop
    .bad_char: ; = end of loop
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ; rdi - pointer
    ; check if '-' is a first symbol
    mov r8b, byte[rdi]
    cmp r8b, '-'
    je .negnum
    jmp .posnum

    .negnum:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

    .posnum:
        call parse_uint
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; rdi - str pointer
    ; rsi - buff pointer
    ; rdx - buff max len
    xor r8, r8 ; len counter, index
    xor r9, r9 ; keep char here

    .loop:
        cmp r8, rdx
        jge .error

        mov r9b, byte[rdi+r8]
        cmp r9b, 0
        je .end

        mov byte[rsi+r8], r9b
        inc r8
        jmp .loop

    .error:
        xor rax, rax
        ret

    .end:
        mov byte[rsi+r8], 0
        inc r8
        mov rax, r8
        ret


    xor rax, rax
    ret

; C : The gun comes in 38 pieces, with a set of assembly instructions.
; After painstakingly assembling the pieces,
; you pull the trigger and the gun promptly backfires
; and blows your head off.

; Assembly : The same as C, except you have to hand-machine all 
; the pieces as well. When you pull the trigger,
; your whole house explodes.